//
//  ViewController.swift
//  MobileWiscWrapper
//
//  Created by Jeffrey on 7/16/15.
//  Copyright (c) 2015 Jeffrey. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //variables
    let mWiscURL = NSURL(string: "http://m.wisc.edu")
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let request = NSURLRequest(URL: mWiscURL!)
        webView.loadRequest(request)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func homeButtonClicked(sender: AnyObject) {
        let request = NSURLRequest(URL: mWiscURL!)
        webView.loadRequest(request)
    }

    
    @IBAction func refreshButtonClicked(sender: AnyObject) {
        
        webView.reload()
    }
    
}

